public class Main {
    public static void main(String[] args) {
        Main main = new Main();
        main.divideByZero(5,0);
    }

    public int divideByZero(int a, int b){
        if(b == 0){
            throw new IllegalArgumentException("Don't divide by 0");
        }
        return a/b;
    }
}
