public enum MoneyType {

    GR(0.01),PLN(1.0),KILO(1000.0);

    private double multiply;

    MoneyType(double multiply) {
        this.multiply = multiply;
    }

    public double getMultiply() {
        return multiply;
    }

}
