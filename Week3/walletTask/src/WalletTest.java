public class WalletTest {
    public static void main(String[] args) {

        Wallet wallet = new Wallet();

        wallet.addCashToWallet(3,MoneyType.KILO);
        System.out.println(wallet);
        wallet.subtractCashFromWallet(30,MoneyType.PLN);
        System.out.println(wallet);
        wallet.addCashToWallet(250,MoneyType.PLN);
        System.out.println(wallet);
        wallet.subtractCashFromWallet(3.5,MoneyType.KILO);


    }
}
