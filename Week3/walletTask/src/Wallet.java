public class Wallet {

    private double amount;

    Wallet() {
        this.amount = 0.0;
    }

    void addCashToWallet(double amount,MoneyType moneyType) {
        addAmount(amount,moneyType);
    }

    private void addAmount(double amount,MoneyType moneyType) {
        this.amount += amount * moneyType.getMultiply();
    }

    void subtractCashFromWallet(double substractAmount,MoneyType moneyType) {
        addAmount(-substractAmount,moneyType);
        if (substractAmount > amount) {
            System.out.println("Nie możesz kupić tego przedmiotu, masz za mało pieniędzy!");
        }
    }

    @Override
    public String toString() {
        return String.format("Obecny stan konta:\n\t GR = %s\n\t PLN = %s\n\t KILO = %s",amount * 100,amount,amount / 1000.0);
    }
}
