/*Stworzyć klasę MathTest zawierającą metodę uruchomieniową main.
        Wykorzystując klasę Math z pakietu java.lang w metodzie main:
        - policzyć i wypisać sinus kąta 45 stopni (PI/4)
        - podać zaokrąglając do liczby całkowitej wynik dzielenia 345/12*/

public class MathTest {
    public static void main(String[] args) {

        printSinDegrees(45);

        printRoundedNumbers(345,12);

    }

    private static void printRoundedNumbers(int a,int b) {
        double numbers = a / b;
        long roundedNumbers = Math.round(numbers);
        System.out.println("Rounded numers: " + roundedNumbers);
    }

    private static void printSinDegrees(int degrees) {

        double angleInRadian = Math.toRadians(degrees);
        double cos = Math.cos(angleInRadian);

        System.out.println("Angle in radians: " + cos);

    }
}
