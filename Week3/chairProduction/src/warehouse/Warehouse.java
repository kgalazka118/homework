package warehouse;

import logistic.ReceiveChairs;

public class Warehouse implements ReceiveChairs {

    private int chairInWarehouse;

    public Warehouse(){
        chairInWarehouse = 0;
    }

    public void warehouseStatus(){
        System.out.println("W magazynie dostępne jest: " + chairInWarehouse + " krzeseł");
    }


    @Override
    public void receive(int chairCount) {
        chairInWarehouse += chairCount;
    }
}
