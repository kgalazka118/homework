package fabric;

import logistic.ReceiveMaterial;

public class Fabric implements ReceiveMaterial {

    private int materialUnitsForChair = 8;
    private int materialUnits;
    private int chairCount;

    public Fabric(){
        materialUnits = 0;
        chairCount = 0;
    }


    @Override
    public void receiveMaterial(int maxOfMaterial) {
        this.materialUnits += maxOfMaterial;
    }

    @Override
    public int directChairs(int maxOfChairs) {
        int chairLoad = Math.min(maxOfChairs, chairCount);
        chairCount -= chairLoad;
        return chairLoad;
    }

    public void produceChairs() {
        chairCount = materialUnits / materialUnitsForChair;
        materialUnits = materialUnits % materialUnitsForChair;
    }


    public void showState() {
        System.out.println("W fabryce jest: " + chairCount + " i pozostałego materiału: " + materialUnits );
    }
}
