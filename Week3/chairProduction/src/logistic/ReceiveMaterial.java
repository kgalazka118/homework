package logistic;

public interface ReceiveMaterial {

    void receiveMaterial (int maxOfMaterial);
    int directChairs (int maxOfChairs);

}
