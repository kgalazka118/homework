package logistic;

import fabric.Fabric;
import warehouse.Warehouse;

public class Logistic {

    private int maxOfChair = 200;
    private int maxOfMaterial = 350;

    private ReceiveMaterial receiveMaterial;
    private ReceiveChairs receiveChairs;

    public Logistic(Warehouse receiveChairs, Fabric receiveMaterial) {
        this.receiveMaterial = receiveMaterial;
        this.receiveChairs = receiveChairs;
    }

    public void deliverMaterialsToFactory() {
        receiveMaterial.receiveMaterial(maxOfMaterial);
        receiveChairs.receive(maxOfChair);

    }

    public void sendChairsFromFactoryToWarehouse() {
        int chairCount = receiveMaterial.directChairs(maxOfChair);
        receiveChairs.receive(chairCount);

    }

}
