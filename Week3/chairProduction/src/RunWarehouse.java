import fabric.Fabric;
import logistic.Logistic;
import warehouse.Warehouse;

public class RunWarehouse {

    public static void main(String[] args) {
        Fabric fabric = new Fabric();
        Warehouse warehouse = new Warehouse();
        Logistic logistic = new Logistic(warehouse, fabric);

        printProducerState(fabric, warehouse, "STAN NA POCZĄTKU");
        logistic.deliverMaterialsToFactory();
        printProducerState(fabric, warehouse, "PO DOSTARCZENIU MATERIAŁU");
        fabric.produceChairs();
        printProducerState(fabric, warehouse, "PO PRODUKCJI KRZESEŁ");
        logistic.sendChairsFromFactoryToWarehouse();
        printProducerState(fabric, warehouse, "PO WYSYŁCE DO MAGAZYNU");

    }

    private static void printProducerState(Fabric factory,Warehouse warehouse,String message) {
        System.out.println("******* "+ message + " ********");
        warehouse.warehouseStatus();
        factory.showState();
    }

}
