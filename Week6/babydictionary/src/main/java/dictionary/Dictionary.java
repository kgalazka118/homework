package dictionary;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

public class Dictionary {

    private Map<String, String> dictionary;

    public Dictionary(Path pathToFile) {
        dictionary = new HashMap<>();
        try {
            Files.readAllLines(pathToFile).forEach(line -> dictionary.put(line.split(",")[0],line.split(",")[1]));
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public Map<String, String> getDictionary() {
        return dictionary;
    }
}
