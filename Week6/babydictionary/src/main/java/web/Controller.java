package web;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    @RequestMapping("/translate")
    public String translate (@RequestParam(value = "coo") String textFromDictionary) {
        return Application.translate(textFromDictionary);
    }

}

