package web;

import dictionary.Dictionary;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.nio.file.Path;
import java.nio.file.Paths;

@SpringBootApplication
public class Application {
    private static Dictionary dictionary;

    public static void main(String[] args) {
        Path pathToFile = Paths.get("src", "main", "resources", "dictionary.txt");
        dictionary = new Dictionary(pathToFile);

        SpringApplication.run(Application.class, args);
    }

    public static String translate(String key) {
        String translator;
        if (dictionary.getDictionary().containsKey(key)) {
            translator = dictionary.getDictionary().get(key);
        } else {
            translator = String.format("Nie ma hasła dla słowa: %s w bazie", key);
        }
        return translator;
    }
}
