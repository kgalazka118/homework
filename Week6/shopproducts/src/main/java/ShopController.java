import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import shop.Product;
import shop.Shop;

public class ShopController {

    private Shop shop;

    @FXML
    protected void initialize(){
        shop = new Shop();
    }

    @FXML
    private TextField productName;

    @FXML
    private TextField productPrice;

    @FXML
    private Label priceError;

    @FXML
    private Button addProduct;

    @FXML
    private VBox shopBox;

    @FXML
    void addProductToShop(ActionEvent event) {
        try{
            String name = productName.getText();
            double price = Double.parseDouble(productPrice.getText());

            shop.addProductToShop(name, price);

            shopBox.getChildren().clear();
            for (Product product :shop.getShop()){
                String valueProduct = String.format("Produkt: %s, Cena: %s", product.getProductName(), product.getProductPrice());
                shopBox.getChildren().add(new Label(valueProduct));
            }
            productName.setText(null);
            productPrice.setText(null);
            priceError.setText(null);

        }
        catch(NumberFormatException numberFormatExcept){
            priceError.setText("Wprowadź prawidłową wartość");
            productPrice.setText("Cena: ");
        }
        catch (Exception exception){
            System.out.println(exception.getMessage());
        }
    }
}
