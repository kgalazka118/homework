package shop;

public class Product implements Comparable<Product> {
    private String productName;
    private double productPrice;

    public Product(String productName,double productPrice) {
        this.productName = productName;
        this.productPrice = productPrice;
    }

    public String getProductName() {
        return productName;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public String printToString(){
        return String.format("Produkt: %s, cena: %s", productName, productPrice);
    }

    public int compareTo(Product product) {
        if (productPrice > product.getProductPrice()){
            return 1;
        }
        else if (productPrice < product.getProductPrice()){
            return -1;
        }
        else {
            return 0;
        }
    }

}
