package shop;

import java.util.TreeSet;

public class Shop {
    private TreeSet<Product> shop;

    public Shop(){ shop = new TreeSet<Product>();}

    public TreeSet<Product> getShop(){
        return shop;
    }

    public void addProductToShop(String productName, double productPrice){
        shop.add(new Product(productName, productPrice));
    }

    public void printShop(){
        for (Product productInt :shop){
            System.out.println(productInt.printToString());
        }
    }
}
