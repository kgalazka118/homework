import java.util.Scanner;

public class PostCodeTest {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Wprowadź kod pocztowy:");

        String postalCode = scanner.next();

        int length = postalCode.length();
        if (length != 6) {
            System.out.println("Twój kod jest błedy. Podaj prawidłowy");
        } else if (postalCode.charAt(2) != '-') {
            System.out.println("Twój kod jest błedy. Podaj prawidłowy");

        } else if (!hasDigits(postalCode)) {
            System.out.println("Twój kod jest błedy. Podaj prawidłowy");
        } else {
            System.out.println("Twój kod jest poprawny.");
        }
    }

    private static boolean hasDigits(String postalCode) {
        char[] chars = postalCode.replace("-","").toCharArray();
        for (char c : chars) {
            boolean digit = Character.isDigit(c);
            if (!digit) {
                return false;
            }
        }
        return true;
    }
}
