import java.util.Scanner;

public class Task5 {

    /*Odczytaj wprowadzony wyraz przez użytkowika i sprawdź czy
    pierwsza litera wyrazu jest taka sama jak ostatnia. Wypisz odpowiedni
    komunikat.*/

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Wprowadź wyraz: ");
        String word = scanner.next();

        char firstLetter = word.charAt(0);
        int wordLength = word.length();
        char lastLetter = word.charAt(wordLength - 1);

        if (firstLetter == lastLetter) {
            System.out.println("Pierwsza i ostatnia litera są takie same");
        } else {
            System.out.println("Pierwsza i ostatnia litera nie są takie same");
        }
    }
}
