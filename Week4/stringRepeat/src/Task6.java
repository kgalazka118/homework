import java.util.Scanner;

public class Task6 {
    /*​Odczytaj imię i nazwisko danej osoby, na ekran wypisz imię i
    nazwisko razem.*/

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj swoje imię: ");
        String name = scanner.next();
        System.out.println("Podaj swoje nazwisko: ");
        String surname = scanner.next();

        System.out.println("Twoje imię i nazwisko to: " + name + " " + surname);
    }
}
