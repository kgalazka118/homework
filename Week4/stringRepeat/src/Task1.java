import java.util.Scanner;

public class Task1 {

    // Odczytaj wyraz i wypisz długość wprowadonego wyrazu.

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Wprowadź wyraz:");
        String word = scanner.next();
        System.out.println("Twój wyraz ma: " + word.length() + " znaki");

    }
}
