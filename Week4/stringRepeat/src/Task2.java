import java.util.Scanner;

public class Task2 {

    /*Odczytaj dany wyraz z konsoli oraz liczbę naturalną n.
    Nasz program powinien zwrócić n ostatnich znaków wyrazu.*/

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Wprowadź słowo: ");
        String word = scanner.next();
        System.out.println("Podaj ile ostatnich liter ma wyświetlić program: ");
        int n = scanner.nextInt();
        int length = word.length();

        System.out.println("Ostatnie litery twojego słowa to: " + word.substring(length - n));

    }
}
