import java.util.Scanner;

public class Task8 {

    // ​Odczytaj 2 wyrazy i sprawdź czy wprowadzone wyrazy są równe.

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj pierwszy wyraz: ");
        String firstWord = scanner.next();
        System.out.println("Podaj drugi wyraz: ");
        String secondWord = scanner.next();
        if(firstWord.equalsIgnoreCase(secondWord)){
            System.out.println("Podane wyrazy są identyczne");
        }
        else {
            System.out.println("Podane wyrazy są różne");
        }
    }
}
