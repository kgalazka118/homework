import java.util.Scanner;

public class Task3 {
    //Odczytaj wyraz i sprawdź czy ostatnia litera to M bądź m.

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Wprowadź wyraz: ");
        String word = scanner.next();
        if (word.endsWith("M")){
            System.out.println("Kończy się na M");
        }
        else if (word.endsWith("m")){
            System.out.println("Kończy się na m");
        }
        else {
            System.out.println("Nie kończy się");
        }

    }
}
