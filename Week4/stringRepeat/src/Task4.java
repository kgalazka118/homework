import java.util.Scanner;

public class Task4 {
    // Odczytaj wyraz i sprawdź czy pierwszy znak Stringa to cyfra.

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Wprowadź wyraz: ");
        String word = scanner.next();

        char firstLetter = word.charAt(0);
        int codeAscii0 = 48;
        int codeAscii9 = 57;

        if (firstLetter >= codeAscii0 && firstLetter <= codeAscii9){
            System.out.println("Twój wyraz zaczyna się od cyfry");
        }
        else{
            System.out.println("Twój wyraz nie zaczyna się od cyfry");
        }
    }
}
