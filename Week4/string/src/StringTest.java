public class StringTest {

    public static void main(String[] args) {

        /*Stworzyć klasę StringTest zawierającą metodę uruchomieniową main.
                W metodzie main:
        - przypisać do zmiennej name swoje nazwisko
        - wypisać je za pomocą System.out.println
                - wypisać z ilu znaków się składa dane nazwisko
                - wypisać pierwszą i ostatnią literę nazwiska
                - wypisać nazwisko WIELKIMI LITERAMI
                - wypisać zdanie (nazwiska w języku polskim się odmienia... ale my sobie to darujemy  ):
„Szanowny Panie {nazwisko}. Jest Pan świetnym programistą.
        Gratuluję!” lub
„Szanowna Pani {nazwisko}. Jest Pani świetną programistką.
        Gratuluję!”.
        - sprawdzić czy pierwsza litera nazwiska występuje w nazwisku jeszcze raz*/


        String name = "Gałązka";
        System.out.println("Twoje nazwisko to: "+ name);
        System.out.println("Twoje nazwisko składa się z " + name.length() + " znaków");

        int nameLength = name.length();
        char lastLetter = name.charAt(nameLength - 1);
        System.out.println("Pierwsza litera nazwiska to: " + name.charAt(0) + ",natomiast ostatnia to: " + lastLetter);
        System.out.println("Wielkie litery: "+ name.toUpperCase());
        System.out.println("\"Szanowny Panie " + name + ". Jest Pan świetnym programistą.\n Gratuluję!\"");
        }
}
